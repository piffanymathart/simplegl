package Examples.Ex07_CanvasSettings;

import SimpleGL.SimpleCanvasGL;
import SimpleGL.SimpleDrawGL;
import SimpleGL.SimpleFrameGL;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Scene: Three overlapping translucent 3D L-shapes that can be interacted with via mouse and keyboard.
 */
class TestMain {

	public static void main(String[] args) {

		String title = "Test GL App";
		TestData data = new TestData();

		// create components
		SimpleCanvasGL canvas = createCanvas(data);
		JMenuBar menuBar = createMenuBar(canvas, data);
		JPanel buttonPanel = createButtonPanel(canvas, data);

		// create containing frame
		Map<Component, String> layout = new HashMap<>();
		layout.put(menuBar, BorderLayout.NORTH);
		layout.put(canvas, BorderLayout.CENTER);
		layout.put(buttonPanel, BorderLayout.WEST);
		CanvasSettings settings = data.getCanvasSettings();
		new SimpleFrameGL(title, new int[] {settings.getWidth(true), settings.getHeight(true)}, layout);

		// give focus to enable key events
		canvas.requestFocusInWindow();
	}

	/**
	 * Creates a canvas with the given data and mouse/keyboard listeners.
	 * @param data The data.
	 * @return The canvas.
	 */
	private static SimpleCanvasGL createCanvas(TestData data) {

		SimpleDrawGL draw = new TestDraw(data);
		SimpleCanvasGL canvas = new SimpleCanvasGL(draw);

		TestCanvasMouseIO mouseIO = new TestCanvasMouseIO(canvas, data);
		canvas.addMouseListener(mouseIO);
		canvas.addMouseMotionListener(mouseIO);
		canvas.addMouseWheelListener(mouseIO);

		TestCanvasKeyboardIO keyboardIO = new TestCanvasKeyboardIO(canvas, data);
		canvas.addKeyListener(keyboardIO);

		return canvas;
	}

	/**
	 * Creates a menu bar.
	 * @return A menu bar.
	 */
	private static JMenuBar createMenuBar(SimpleCanvasGL canvas, TestData data) {

		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Overlay");

		JMenuItem noneMenuItem = new JMenuItem("None");
		noneMenuItem.addActionListener(new TestCanvasMenuIO(canvas, data, null));

		JMenuItem redMenuItem = new JMenuItem("Red");
		redMenuItem.addActionListener(new TestCanvasMenuIO(canvas, data, Color.red));

		JMenuItem greenMenuItem = new JMenuItem("Green");
		greenMenuItem.addActionListener(new TestCanvasMenuIO(canvas, data, Color.green));

		JMenuItem blueMenuItem = new JMenuItem("Blue");
		blueMenuItem.addActionListener(new TestCanvasMenuIO(canvas, data, Color.blue));

		menu.add(noneMenuItem);
		menu.add(redMenuItem);
		menu.add(greenMenuItem);
		menu.add(blueMenuItem);
		menuBar.add(menu);

		return menuBar;
	}

	/**
	 * Creates a button panel.
	 * @return A button panel.
	 */
	private static JPanel createButtonPanel(SimpleCanvasGL canvas, TestData data) {

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(3,1));

		JButton button1 = new JButton("w=1");
		button1.addActionListener(new TestCanvasButtonIO(canvas, data, 1));

		JButton button2 = new JButton("w=2");
		button2.addActionListener(new TestCanvasButtonIO(canvas, data, 2));

		JButton button3 = new JButton("w=10");
		button3.addActionListener(new TestCanvasButtonIO(canvas, data, 10));

		buttonPanel.add(button1);
		buttonPanel.add(button2);
		buttonPanel.add(button3);

		return buttonPanel;
	}
}
