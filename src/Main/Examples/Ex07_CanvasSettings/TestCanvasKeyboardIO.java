package Examples.Ex07_CanvasSettings;

import SimpleGL.SimpleCanvasGL;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.*;

/**
 * Managers IO operations with the canvas.
 *
 * Key press: A/D selects the polygon to the left/right of the current selected. SPACE shows/hides the polygon fill.
 */
class TestCanvasKeyboardIO implements KeyListener {

	private SimpleCanvasGL m_canvas;
	private TestData m_data;

	TestCanvasKeyboardIO(SimpleCanvasGL canvas, TestData data) {
		m_canvas = canvas;
		m_data = data;
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
			case VK_A:
				m_data.incrementSelected(-1);
				m_canvas.display();
				break;
			case VK_D:
				m_data.incrementSelected(1);
				m_canvas.display();
				break;
			case VK_SPACE:
				m_data.toggleShowSelected();
				m_canvas.display();
				break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}
}
