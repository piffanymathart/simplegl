package Examples.Ex07_CanvasSettings;

import java.awt.*;

/**
 * The data object containing canvas settings.
 */
class CanvasSettings {

	/**
	 * The overlay tint/colour.
	 */
	private Color m_overlayTint;

	/**
	 * The line width.
	 */
	private int m_lineWidth;

	/**
	 * The canvas width.
	 */
	private int m_width;

	/**
	 * The canvas height.
	 */
	private int m_height;

	/**
	 * The DPI scale.
	 */
	private double m_dpiScale;

	/**
	 * Default constructor to initialize the canvas settings.
	 */
	CanvasSettings() {
		m_width = 640;
		m_height = 480;
		m_dpiScale = 1;
		m_overlayTint = null;
		m_lineWidth = 1;
	}

	/**
	 * Sets the overlay tint.
	 * @param c The overlay tint to set to.
	 */
	void setOverlayTint(Color c) {
		m_overlayTint = c;
	}

	/**
	 * Gets the overlay tint.
	 * @return The overlay tint.
	 */
	Color getOverlayTint() {
		return m_overlayTint;
	}

	/**
	 * Sets the line width.
	 * @param lineWidth The line width.
	 */
	void setLineWidth(int lineWidth) { m_lineWidth = lineWidth; }

	/**
	 * Gets the line width.
	 * @return The line width.
	 */
	int getLineWidth() { return m_lineWidth; }

	/**
	 * Gets the canvas width.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The width.
	 */
	int getWidth(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_width*m_dpiScale) : m_width;
	}

	/**
	 * Gets the canvas height.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The height.
	 */
	int getHeight(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_height*m_dpiScale) : m_height;
	}

	/**
	 * Sets the canvas width.
	 * @param width The width.
	 */
	void setWidth(int width) { m_width = width; }

	/**
	 * Sets the canvas height.
	 * @param height The height.
	 */
	void setHeight(int height) { m_height = height; }

	/**
	 * Gets the DPI scale.
	 * @return The DPI scale.
	 */
	double getDpiScale() { return m_dpiScale; }

	/**
	 * Sets the DPI scale.
	 * @param dpiScale The DPI scale.
	 */
	public void setDpiScale(double dpiScale) { m_dpiScale = dpiScale; }
}
