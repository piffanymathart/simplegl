package Examples.Ex07_CanvasSettings;

import SimpleGL.SimpleCanvasGL;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TestCanvasMenuIO implements ActionListener {

	private SimpleCanvasGL m_canvas;
	private TestData m_data;
	private Color m_color;

	TestCanvasMenuIO(SimpleCanvasGL canvas, TestData data, Color color) {
		m_canvas = canvas;
		m_data = data;
		m_color = color;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		CanvasSettings settings = m_data.getCanvasSettings();
		settings.setOverlayTint(m_color);
		m_data.setCanvasSettings(settings);
		m_canvas.display();
	}
}
