package Examples.Ex07_CanvasSettings;

import SimpleGL.SimpleCanvasGL;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class TestCanvasButtonIO implements ActionListener {

	private SimpleCanvasGL m_canvas;
	private TestData m_data;
	private int m_lineWidth;

	TestCanvasButtonIO(SimpleCanvasGL canvas, TestData data, int lineWidth) {
		m_canvas = canvas;
		m_data = data;
		m_lineWidth = lineWidth;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		CanvasSettings settings = m_data.getCanvasSettings();
		settings.setLineWidth(m_lineWidth);
		m_data.setCanvasSettings(settings);
		m_canvas.display();
	}
}
