package Examples.Ex07_CanvasSettings;

import SimpleGL.GLDrawHelper;
import SimpleGL.GLSetupHelper;
import SimpleGL.SimpleDrawGL;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import javax.vecmath.Point3d;
import java.awt.*;

/**
 * Setup: 3D setup
 * Draw: Three overlapping translucent 3D L-shapes that can be interacted with via mouse and keyboard.
 */
class TestDraw extends SimpleDrawGL {

	/**
	 * The data object.
	 */
	private TestData m_data;

	/**
	 * Constructs from data.
	 * @param data The data object.
	 */
	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		CanvasSettings settings = m_data.getCanvasSettings();
		settings.setWidth(width);
		settings.setHeight(height);
		settings.setDpiScale(dpiScale);
		m_data.setCanvasSettings(settings);
	}

	@Override
	public void setup(GL2 gl2) {

		GLSetupHelper.enableTranslucency(gl2);
		GLSetupHelper.enableAntialiasing(gl2);
		CanvasSettings settings = m_data.getCanvasSettings();
		GLSetupHelper.set3DMode(gl2, settings.getWidth(true), settings.getHeight(true), Math.PI/180.0*35.0);
	}

	@Override
	public void draw(GL2 gl2) {

		gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
		gl2.glLoadIdentity();

		CanvasSettings settings = m_data.getCanvasSettings();

		for(int i=0; i<m_data.getNumPolygons(); i++) {
			Point3d[] polygon = m_data.getPolygon(i);
			double[] fc = m_data.getFillColour(i);
			double[] sc = m_data.getStrokeColour(i);
			int lineWidth = settings.getLineWidth();
			gl2.glLineWidth(lineWidth);
			gl2.glColor4d( fc[0], fc[1], fc[2], fc[3] );
			GLDrawHelper.fillPolygon3d(gl2, polygon);
			gl2.glColor4d( sc[0], sc[1], sc[2], sc[3] );
			GLDrawHelper.drawPolygon3d(gl2, polygon);
		}

		// overlay
		Color overlay = settings.getOverlayTint();
		if(overlay != null) {
			// overlay
			Point3d[] rect = {
				new Point3d(-1, -1, -0.1),
				new Point3d(-1, 1, -0.1),
				new Point3d(1, 1, -0.1),
				new Point3d(1, -1, -0.1)
			};
			gl2.glColor4d(overlay.getRed()/255.0, overlay.getGreen()/255.0, overlay.getBlue()/225.0,0.1);
			GLDrawHelper.fillPolygon3d(gl2, rect);
		}
	}
}
