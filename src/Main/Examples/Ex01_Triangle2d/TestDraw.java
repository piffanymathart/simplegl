package Examples.Ex01_Triangle2d;

import SimpleGL.SimpleDrawGL;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;

/**
 * Setup: 2D setup
 * Draw: A 2D triangle spanning the canvas, with smooth interpolated colours between the vertices.
 */
class TestDraw extends SimpleDrawGL {

	private TestData m_data;

	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		m_data.setWidth(width);
		m_data.setHeight(height);
		m_data.setDpiScale(dpiScale);
	}

	@Override
	public void setup(GL2 gl2) {
		gl2.glMatrixMode( GL2.GL_PROJECTION );
		gl2.glLoadIdentity();

		int w = m_data.getWidth(true);
		int h = m_data.getHeight(true);

		// coordinate system origin at lower left with width and height same as the window
		GLU glu = new GLU();
		glu.gluOrtho2D( 0.0, w, 0.0, h );

		gl2.glMatrixMode( GL2.GL_MODELVIEW );
		gl2.glLoadIdentity();

		gl2.glViewport( 0, 0, w, h );
	}

	@Override
	public void draw(GL2 gl2) {

		int w = m_data.getWidth(true);
		int h = m_data.getHeight(true);

		gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
		gl2.glLoadIdentity();

		gl2.glBegin( GL.GL_TRIANGLES );
		gl2.glColor3d( 1, 0, 0 );
		gl2.glVertex2d( 0, 0 );
		gl2.glColor3d( 0, 1, 0 );
		gl2.glVertex2d( w, 0 );
		gl2.glColor3d( 0, 0, 1 );
		gl2.glVertex2d( w/2.0, h );
		gl2.glEnd();
	}
}
