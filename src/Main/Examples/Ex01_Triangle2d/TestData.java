package Examples.Ex01_Triangle2d;

/**
 * Contains data to be drawn.
 */
class TestData {

	/**
	 * The canvas width.
	 */
	private int m_width = 640;

	/**
	 * The canvas height.
	 */
	private int m_height = 480;

	/**
	 * The DPI scale.
	 */
	private double m_dpiScale = 1;

	/**
	 * Gets the canvas width.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The width.
	 */
	int getWidth(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_width*m_dpiScale) : m_width;
	}

	/**
	 * Gets the canvas height.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The height.
	 */
	int getHeight(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_height*m_dpiScale) : m_height;
	}

	/**
	 * Sets the canvas width.
	 * @param width The width.
	 */
	void setWidth(int width) { m_width = width; }

	/**
	 * Sets the canvas height.
	 * @param height The height.
	 */
	void setHeight(int height) { m_height = height; }

	/**
	 * Gets the DPI scale.
	 * @return The DPI scale.
	 */
	double getDpiScale() { return m_dpiScale; }

	/**
	 * Sets the DPI scale.
	 * @param dpiScale The DPI scale.
	 */
	void setDpiScale(double dpiScale) { m_dpiScale = dpiScale; }
}
