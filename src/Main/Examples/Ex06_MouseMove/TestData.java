package Examples.Ex06_MouseMove;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.Arrays;
import java.util.List;

/**
 * Contains data to be drawn.
 */
class TestData {

	/**
	 * The polygons.
	 */
	private List<Point3d[]> m_polygons;

	/**
	 * The rotational centres of the polygons.
	 */
	private List<Point3d> m_centres;

	/**
	 * The fill colours.
	 */
	private List<double[]> m_fillColours;

	/**
	 * The stroke colours.
	 */
	private List<double[]> m_strokeColours;

	/**
	 * The index of the selected polygon.
	 */
	private int m_selected = 0;

	/**
	 * The fill/stroke alpha values of unselected polygons.
	 */
	private final double[] unselectedAlphas = {0.2, 0.4};

	/**
	 * The fill/stroke alpha values of selected polygons.
	 */
	private final double[] selectedAlphas = {0.4, 0.8};

	/**
	 * The show/hide statuses of the polygons.
	 */
	private List<Boolean> m_show;

	/**
	 * The canvas width.
	 */
	private int m_width;

	/**
	 * The canvas height.
	 */
	private int m_height;

	/**
	 * The DPI scale.
	 */
	private double m_dpiScale = 1;

	/**
	 * Default constructor to initialize the data.
	 */
	TestData() {

		m_width = 640;
		m_height = 480;

		Point3d[] polygon1 = getLShape(-1.5, -1, -6, 0.5, 0.5);
		Point3d[] polygon2 = getLShape(0, 0, -6, 2, 2);
		Point3d[] polygon3 = getLShape(0.5, 0.3, -6, 1.5, 3);

		m_polygons = Arrays.asList(polygon1, polygon2, polygon3);

		m_centres = Arrays.asList(
			new Point3d(-1.5,-1,-6),
			new Point3d(0,0,-6),
			new Point3d(0.5,0.3,-6)
		);

		double[] fc1 = {1, 0, 0};
		double[] fc2 = {0, 1, 0};
		double[] fc3 = {0, 0, 1};

		m_fillColours = Arrays.asList(fc1, fc2, fc3);

		double[] sc1 = {1, 0, 0};
		double[] sc2 = {0, 1, 0};
		double[] sc3 = {0, 0, 1};

		m_strokeColours = Arrays.asList(sc1, sc2, sc3);

		m_show = Arrays.asList(true, true, true);
	}

	/**
	 * Gets the canvas width.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The width.
	 */
	int getWidth(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_width*m_dpiScale) : m_width;
	}

	/**
	 * Gets the canvas height.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The height.
	 */
	int getHeight(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_height*m_dpiScale) : m_height;
	}

	/**
	 * Sets the canvas width.
	 * @param width The width.
	 */
	void setWidth(int width) { m_width = width; }

	/**
	 * Sets the canvas height.
	 * @param height The height.
	 */
	void setHeight(int height) { m_height = height; }

	/**
	 * Gets the DPI scale.
	 * @return The DPI scale.
	 */
	double getDpiScale() { return m_dpiScale; }

	/**
	 * Sets the DPI scale.
	 * @param dpiScale The DPI scale.
	 */
	void setDpiScale(double dpiScale) { m_dpiScale = dpiScale; }

	/**
	 * Gets the specified polygon.
	 * @param index The index.
	 * @return The specified polygon.
	 */
	Point3d[] getPolygon(int index) {
		return m_polygons.get(index);
	}

	/**
	 * Gets the specified fill colour.
	 * @param index The index.
	 * @return The specified fill colour.
	 */
	double[] getFillColour(int index) {
		double[] c = m_fillColours.get(index);
		double a = m_show.get(index)
			? (index==m_selected) ? selectedAlphas[0] : unselectedAlphas[0]
			: 0;
		return new double[] {c[0], c[1], c[2], a};
	}

	/**
	 * Gets the specified stroke colour.
	 * @param index The index.
	 * @return The specified stroke colour.
	 */
	double[] getStrokeColour(int index) {
		double[] c = m_strokeColours.get(index);
		double a = (index==m_selected) ? selectedAlphas[1] : unselectedAlphas[1];
		return new double[] {c[0], c[1], c[2], a};
	}

	/**
	 * Gets the number of polygons.
	 * @return The number of polygons.
	 */
	int getNumPolygons() {
		return m_polygons.size();
	}

	/**
	 * Increments the index of the selected polygon by val.
	 */
	void incrementSelected(int val) {
		m_selected = (m_selected + val + m_polygons.size()) % m_polygons.size();
	}

	/**
	 * Toggles the selected polygon's show/hide status.
	 */
	void toggleShowSelected() {
		m_show.set(m_selected, !m_show.get(m_selected));
	}

	/**
	 * Rotates the selected polygon.
	 * @param axis The rotation axis ('x', 'y', or 'z')
	 * @param angle The angle (in radians) by which to rotate.
	 */
	void rotateSelectedPolygon(char axis, double angle) {
		Point3d[] polygon = m_polygons.get(m_selected);
		Point3d centre = m_centres.get(m_selected);

		Matrix4d translate = new Matrix4d();
		translate.setIdentity();
		translate.setTranslation(new Vector3d(-centre.x, -centre.y, -centre.z));

		Matrix4d rotate = new Matrix4d();
		rotate.setIdentity();
		switch(axis) {
			case 'x': rotate.rotX(angle); break;
			case 'y': rotate.rotY(angle); break;
			case 'z': rotate.rotZ(angle); break;
		}

		Matrix4d translateBack = new Matrix4d();
		translateBack.setIdentity();
		translateBack.setTranslation(new Vector3d(centre.x, centre.y, centre.z));

		for(int i=0; i<polygon.length; i++) {
			Point3d p = polygon[i];
			translate.transform(p);
			rotate.transform(p);
			translateBack.transform(p);
			polygon[i] = p;
		}
	}

	/**
	 * Gets the vertices of a 3D L-shape given the centre position and the dimension.
	 * @param x The x-value of the centre.
	 * @param y The y-value of the centre.
	 * @param w The width of the triangle.
	 * @param h The height of the triangle.
	 * @return The vertices of the L-shape.
	 */
	private Point3d[] getLShape(double x, double y, double z, double w, double h) {
		Point3d[] polygon = {
			new Point3d(-w/2.0 + x , -h/2.0 + y, z),
			new Point3d(0 + x, -h/2.0 + y, z),
			new Point3d(0 + x, 0 + y, z),
			new Point3d(w/2.0 + x, 0 + y, z),
			new Point3d(w/2.0 + x, h/2.0 + y, z),
			new Point3d(-w/2.0 + x, h/2.0 + y, z)
		};
		return polygon;
	}
}
