package Examples.Ex06_MouseMove;

import SimpleGL.GLDrawHelper;
import SimpleGL.GLSetupHelper;
import SimpleGL.SimpleDrawGL;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import javax.vecmath.Point3d;


class TestDraw extends SimpleDrawGL {

	private TestData m_data;

	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		m_data.setWidth(width);
		m_data.setHeight(height);
		m_data.setDpiScale(dpiScale);
	}

	@Override
	public void setup(GL2 gl2) {

		GLSetupHelper.enableTranslucency(gl2);
		GLSetupHelper.enableAntialiasing(gl2);
		GLSetupHelper.set3DMode(gl2, m_data.getWidth(true), m_data.getHeight(true), Math.PI/180.0*35.0);
	}

	@Override
	public void draw(GL2 gl2) {

		gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
		gl2.glLoadIdentity();

		for(int i=0; i<m_data.getNumPolygons(); i++) {
			Point3d[] polygon = m_data.getPolygon(i);
			double[] fc = m_data.getFillColour(i);
			double[] sc = m_data.getStrokeColour(i);
			gl2.glColor4d( fc[0], fc[1], fc[2], fc[3] );
			GLDrawHelper.fillPolygon3d(gl2, polygon);
			gl2.glColor4d( sc[0], sc[1], sc[2], sc[3] );
			GLDrawHelper.drawPolygon3d(gl2, polygon);
		}
	}
}
