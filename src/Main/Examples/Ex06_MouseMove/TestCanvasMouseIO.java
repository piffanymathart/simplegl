package Examples.Ex06_MouseMove;

import SimpleGL.SimpleCanvasGL;

import javax.swing.*;
import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import java.awt.event.*;

/**
 * Managers IO operations with the canvas.
 *
 * Mouse drag: left button rotates the selected polygons about z-axis, and right button rotates it about y-axis.
 * Mouse wheel: scrolling rotates the selected polygons about x-axis.
 */
class TestCanvasMouseIO implements MouseListener, MouseMotionListener, MouseWheelListener {

	private SimpleCanvasGL m_canvas;
	private TestData m_data;

	private Point2d[] lastDown = {null, null, null};

	TestCanvasMouseIO(SimpleCanvasGL canvas, TestData data) {
		m_canvas = canvas;
		m_data = data;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		recordMouseDown(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		recordMouseUp(e);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// left mouse rotates about z-axis
		if(SwingUtilities.isLeftMouseButton(e)) {
			Vector2d moved = new Vector2d(e.getX(), e.getY());
			moved.sub(lastDown[0]);
			double angleY = moved.x / (double)getWidth() * 2.0 * Math.PI;
			m_data.rotateSelectedPolygon('y', angleY);
			m_canvas.display();
		}
		// right button rotates about y-axis
		if(SwingUtilities.isRightMouseButton(e)) {
			Point2d canvasCentre = new Point2d((double)getWidth()/2.0, (double)getHeight()/2.0);
			double angle = getRotationAngleZ(
				canvasCentre,
				lastDown[2],
				new Point2d(e.getX(), e.getY())
			);
			m_data.rotateSelectedPolygon('z', angle);
			m_canvas.display();
		}
		recordMouseDown(e);
	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// mouse wheel scroll rotates about x-axis
		double angle = (double) e.getWheelRotation() * 0.3;
		m_data.rotateSelectedPolygon('x', angle);
		m_canvas.display();
	}

	private void recordMouseDown(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)) {
			lastDown[0] = new Point2d(e.getX(), e.getY());
		}
		else if(SwingUtilities.isMiddleMouseButton(e)) {
			lastDown[1] = new Point2d(e.getX(), e.getY());
		}
		else if(SwingUtilities.isRightMouseButton(e)) {
			lastDown[2] = new Point2d(e.getX(), e.getY());
		}
	}

	private void recordMouseUp(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)) {
			lastDown[0] = null;
		}
		else if(SwingUtilities.isMiddleMouseButton(e)) {
			lastDown[1] = null;
		}
		else if(SwingUtilities.isRightMouseButton(e)) {
			lastDown[2] = null;
		}
	}

	private double getRotationAngleZ(Point2d centre, Point2d from, Point2d to) {

		if(centre.distance(from) < 1e-5 || centre.distance(to) < 1e-5) return 0;

		Vector2d v1 = new Vector2d(from);
		v1.sub(centre);
		v1.normalize();

		Vector2d v2 = new Vector2d(to);
		v2.sub(centre);
		v2.normalize();

		double dot = Math.min(1, Math.max(-1, v1.dot(v2)));

		double angle = Math.acos(dot);

		Vector3d cross = new Vector3d();
		cross.cross(new Vector3d(v1.x,v1.y,0), new Vector3d(v2.x,v2.y,0));

		if(cross.z > 0) angle = -angle;

		return angle;
	}

	/**
	 * Gets the unscaled canvas width.
	 * @return The unscaled canvas width.
	 */
	private int getWidth() {
		return m_data.getWidth(false);
	}

	/**
	 * Gets the unscaled canvas height.
	 * @return The unscaled canvas height.
	 */
	private int getHeight() {
		return m_data.getHeight(false);
	}
}
