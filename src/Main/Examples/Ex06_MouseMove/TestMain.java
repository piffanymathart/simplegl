package Examples.Ex06_MouseMove;

import SimpleGL.SimpleCanvasGL;
import SimpleGL.SimpleDrawGL;
import SimpleGL.SimpleFrameGL;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Scene: Three overlapping translucent 3D L-shapes that can be interacted with via mouse and keyboard.
 */
class TestMain {

	public static void main(String[] args) {

		String title = "Test GL App";
		TestData data = new TestData();

		// create components
		JMenuBar menuBar = createMenuBar();
		SimpleCanvasGL canvas = createCanvas(data);
		JPanel buttonPanel = createButtonPanel();

		// create containing frame
		Map<Component, String> layout = new HashMap<>();
		layout.put(menuBar, BorderLayout.NORTH);
		layout.put(canvas, BorderLayout.CENTER);
		layout.put(buttonPanel, BorderLayout.WEST);
		new SimpleFrameGL(title, new int[] {data.getWidth(true), data.getHeight(true)}, layout);

		// give focus to enable key events
		canvas.requestFocusInWindow();
	}

	/**
	 * Creates a canvas with the given data and mouse/keyboard listeners.
	 * @param data The data.
	 * @return The canvas.
	 */
	private static SimpleCanvasGL createCanvas(TestData data) {

		SimpleDrawGL draw = new TestDraw(data);
		SimpleCanvasGL canvas = new SimpleCanvasGL(draw);


		TestCanvasMouseIO mouseIO = new TestCanvasMouseIO(canvas, data);
		canvas.addMouseListener(mouseIO);
		canvas.addMouseMotionListener(mouseIO);
		canvas.addMouseWheelListener(mouseIO);

		TestCanvasKeyboardIO keyboardIO = new TestCanvasKeyboardIO(canvas, data);
		canvas.addKeyListener(keyboardIO);

		return canvas;
	}

	/**
	 * Creates a menu bar.
	 * @return A menu bar.
	 */
	private static JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(new JMenu("Apple"));
		menuBar.add(new JMenu("Banana"));
		menuBar.add(new JMenu("Carrot"));
		return menuBar;
	}

	/**
	 * Creates a button panel.
	 * @return A button panel.
	 */
	private static JPanel createButtonPanel() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(3,1));
		buttonPanel.add(new JButton("A"));
		buttonPanel.add(new JButton("B"));
		buttonPanel.add(new JButton("C"));
		return buttonPanel;
	}
}
