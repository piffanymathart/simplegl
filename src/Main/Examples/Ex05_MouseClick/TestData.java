package Examples.Ex05_MouseClick;

import javax.vecmath.Point3d;
import java.util.Arrays;
import java.util.List;

/**
 * Contains data to be drawn.
 */
class TestData {

	/**
	 * The polygons.
	 */
	private List<Point3d[]> m_polygons;

	/**
	 * The fill colours.
	 */
	private List<double[]> m_fillColours;

	/**
	 * The stroke colours.
	 */
	private List<double[]> m_strokeColours;

	/**
	 * Number of polygons to draw.
	 */
	private int m_numDraw = 0;

	/**
	 * The canvas width.
	 */
	private int m_width;

	/**
	 * The canvas height.
	 */
	private int m_height;

	/**
	 * The DPI scale.
	 */
	private double m_dpiScale = 1;

	/**
	 * Default constructor to initialize the data.
	 */
	TestData() {

		m_width = 640;
		m_height = 480;

		Point3d[] polygon1 = getLShape(0, 0, -6, 2, 2);
		Point3d[] polygon2 = getLShape(0.5, 0.3, -6, 1.5, 3);
		Point3d[] polygon3 = getLShape(-1.5, -1, -6, 0.5, 0.5);

		m_polygons = Arrays.asList(polygon1, polygon2, polygon3);

		double[] fc1 = {1, 0, 0, 0.2};
		double[] fc2 = {0, 1, 0, 0.2};
		double[] fc3 = {0, 0, 1, 0.2};

		m_fillColours = Arrays.asList(fc1, fc2, fc3);

		double[] sc1 = {1, 0, 0, 0.7};
		double[] sc2 = {0, 1, 0, 0.7};
		double[] sc3 = {0, 0, 1, 0.7};

		m_strokeColours = Arrays.asList(sc1, sc2, sc3);

		m_numDraw = m_polygons.size();
	}

	/**
	 * Gets the canvas width.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The width.
	 */
	int getWidth(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_width*m_dpiScale) : m_width;
	}

	/**
	 * Gets the canvas height.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The height.
	 */
	int getHeight(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_height*m_dpiScale) : m_height;
	}

	/**
	 * Sets the canvas width.
	 * @param width The width.
	 */
	void setWidth(int width) { m_width = width; }

	/**
	 * Sets the canvas height.
	 * @param height The height.
	 */
	void setHeight(int height) { m_height = height; }

	/**
	 * Gets the DPI scale.
	 * @return The DPI scale.
	 */
	double getDpiScale() { return m_dpiScale; }

	/**
	 * Sets the DPI scale.
	 * @param dpiScale The DPI scale.
	 */
	void setDpiScale(double dpiScale) { m_dpiScale = dpiScale; }

	/**
	 * Increments the number of polygons to draw. Ranges from 0 to the number of polygons, then repeats.
	 */
	void incrementNumDraw() {
		m_numDraw = (m_numDraw +1)%(m_polygons.size()+1);
	}

	/**
	 * Gets the specified polygon.
	 * @param index The index.
	 * @return The specified polygon.
	 */
	Point3d[] getPolygon(int index) {
		return m_polygons.get(index);
	}

	/**
	 * Gets the specified fill colour.
	 * @param index The index.
	 * @return The specified fill colour.
	 */
	double[] getFillColour(int index) {
		return m_fillColours.get(index);
	}

	/**
	 * Gets the specified stroke colour.
	 * @param index The index.
	 * @return The specified stroke colour.
	 */
	double[] getStrokeColour(int index) {
		return m_strokeColours.get(index);
	}

	/**
	 * Gets the number of polygons to draw.
	 * @return The number of polygons to draw.
	 */
	public int getNumDraw() {
		return m_numDraw;
	}

	/**
	 * Gets the vertices of a 3D L-shape given the centre position and the dimension.
	 * @param x The x-value of the centre.
	 * @param y The y-value of the centre.
	 * @param w The width of the triangle.
	 * @param h The height of the triangle.
	 * @return The vertices of the L-shape.
	 */
	private Point3d[] getLShape(double x, double y, double z, double w, double h) {
		Point3d[] polygon = {
			new Point3d(-w/2.0 + x , -h/2.0 + y, z),
			new Point3d(0 + x, -h/2.0 + y, z),
			new Point3d(0 + x, 0 + y, z),
			new Point3d(w/2.0 + x, 0 + y, z),
			new Point3d(w/2.0 + x, h/2.0 + y, z),
			new Point3d(-w/2.0 + x, h/2.0 + y, z)
		};
		return polygon;
	}
}
