package Examples.Ex05_MouseClick;

import SimpleGL.SimpleCanvasGL;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Managers IO operations with the canvas.
 *
 * Mouse click: increments the number of polygons drawn.
 */
class TestCanvasMouseIO implements MouseListener {

	private SimpleCanvasGL m_canvas;
	private TestData m_data;

	TestCanvasMouseIO(SimpleCanvasGL canvas, TestData data) {
		m_canvas = canvas;
		m_data = data;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		m_data.incrementNumDraw();
		m_canvas.display();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}
}
