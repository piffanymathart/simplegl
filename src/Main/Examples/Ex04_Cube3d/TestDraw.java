package Examples.Ex04_Cube3d;

import SimpleGL.GLSetupHelper;
import SimpleGL.SimpleDrawGL;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

/**
 * Setup: 3D setup
 * Draw: Three overlapping translucent faces of a 3D cube.
 */
class TestDraw extends SimpleDrawGL {

	private TestData m_data;

	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		m_data.setWidth(width);
		m_data.setHeight(height);
		m_data.setDpiScale(dpiScale);
	}

	@Override
	public void setup(GL2 gl2) {

		GLSetupHelper.enableTranslucency(gl2);
		GLSetupHelper.enableAntialiasing(gl2);
		GLSetupHelper.set3DMode(gl2, m_data.getWidth(true), m_data.getHeight(true), Math.PI/180.0*35.0);
	}

	@Override
	public void draw(GL2 gl2) {

		gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
		gl2.glLoadIdentity();

		int z = -6;

		// front face
		gl2.glColor4d( 1, 0, 0, 0.5 );
		gl2.glBegin( GL.GL_TRIANGLE_FAN );
		gl2.glVertex3d( -1, -1, z );
		gl2.glVertex3d( 1, -1, z );
		gl2.glVertex3d( 1, 1, z );
		gl2.glVertex3d( -1, 1, z );
		gl2.glEnd();

		// left face
		gl2.glColor4d( 0, 1, 0, 0.5 );
		gl2.glBegin( GL.GL_TRIANGLE_FAN );
		gl2.glVertex3d( -1, -1, z );
		gl2.glVertex3d( -1, 1, z );
		gl2.glVertex3d( -1, 1, z-1 );
		gl2.glVertex3d( -1, -1, z-1 );
		gl2.glEnd();

		// top face
		gl2.glColor4d( 0, 0, 1, 0.5 );
		gl2.glBegin( GL.GL_TRIANGLE_FAN );
		gl2.glVertex3d( -1, 1, z );
		gl2.glVertex3d( 1, 1, z );
		gl2.glVertex3d( 1, 1, z-1 );
		gl2.glVertex3d( -1, 1, z-1 );
		gl2.glEnd();
	}
}
