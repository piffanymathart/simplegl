package Examples.Ex04_Cube3d;

import SimpleGL.SimpleCanvasGL;
import SimpleGL.SimpleDrawGL;
import SimpleGL.SimpleFrameGL;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Scene: Three overlapping translucent faces of a 3D cube.
 */
class TestMain {

	public static void main(String[] args) {

		String title = "Test GL App";
		TestData data = new TestData();

		Map<Component, String> layoutComponentMap = new HashMap<>();

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(new JMenu("Apple"));
		menuBar.add(new JMenu("Banana"));
		menuBar.add(new JMenu("Carrot"));
		layoutComponentMap.put(menuBar, BorderLayout.NORTH);

		SimpleDrawGL draw = new TestDraw(data);
		SimpleCanvasGL canvas = new SimpleCanvasGL(draw);
		layoutComponentMap.put( canvas, BorderLayout.CENTER );

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(3,1));
		buttonPanel.add(new JButton("A"));
		buttonPanel.add(new JButton("B"));
		buttonPanel.add(new JButton("C"));
		layoutComponentMap.put(buttonPanel, BorderLayout.WEST);

		new SimpleFrameGL(title, new int[] {data.getWidth(true), data.getHeight(true)}, layoutComponentMap);
	}
}
