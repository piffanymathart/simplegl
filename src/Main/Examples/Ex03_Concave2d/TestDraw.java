package Examples.Ex03_Concave2d;

import SimpleGL.GLDrawHelper;
import SimpleGL.GLSetupHelper;
import SimpleGL.SimpleDrawGL;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import javax.vecmath.Point2d;

/**
 * Setup: 2D setup
 * Draw: Three overlapping translucent 2D concave L-shapes.
 */
class TestDraw extends SimpleDrawGL {

	private TestData m_data;

	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		m_data.setWidth(width);
		m_data.setHeight(height);
		m_data.setDpiScale(dpiScale);
	}

	@Override
	public void setup(GL2 gl2) {

		GLSetupHelper.enableTranslucency(gl2);
		GLSetupHelper.enableAntialiasing(gl2);
		GLSetupHelper.set2DMode(gl2, m_data.getWidth(true), m_data.getHeight(true));
	}

	@Override
	public void draw(GL2 gl2) {

		gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
		gl2.glLoadIdentity();

		int w = m_data.getWidth(true);
		int h = m_data.getHeight(true);

		// red
		Point2d[] polygon1 = getLShape(w/2.0, h/2.0, w/2.0, h/2.0);
		gl2.glColor4d( 1, 0, 0, 0.2 );
		GLDrawHelper.fillPolygon2d(gl2, polygon1);
		gl2.glColor4d( 1, 0, 0, 0.7 );
		GLDrawHelper.drawPolygon2d(gl2, polygon1);

		// green
		Point2d[] polygon2 = getLShape(w*0.75, h/2.0, w/3.0, h*0.75);
		gl2.glColor4d( 0, 1, 0, 0.2 );
		GLDrawHelper.fillPolygon2d(gl2, polygon2);
		gl2.glColor4d( 0, 1, 0, 0.7 );
		GLDrawHelper.drawPolygon2d(gl2, polygon2);

		// blue
		Point2d[] polygon3 = getLShape(400, 400, 400, 400);
		gl2.glColor4d( 0, 0, 1, 0.2 );
		GLDrawHelper.fillPolygon2d(gl2, polygon3);
		gl2.glColor4d( 0, 0, 1, 0.7 );
		GLDrawHelper.drawPolygon2d(gl2, polygon3);
	}

	/**
	 * Gets the vertices of a 2D L-shape given the centre position and the dimension.
	 * @param x The x-value of the centre.
	 * @param y The y-value of the centre.
	 * @param w The width of the triangle.
	 * @param h The height of the triangle.
	 * @return The vertices of the L-shape.
	 */
	private Point2d[] getLShape(double x, double y, double w, double h) {
		Point2d[] polygon = {
			new Point2d(-w/2.0 + x , -h/2.0 + y),
			new Point2d(0 + x, -h/2.0 + y),
			new Point2d(0 + x, 0 + y),
			new Point2d(w/2.0 + x, 0 + y),
			new Point2d(w/2.0 + x, h/2.0 + y),
			new Point2d(-w/2.0 + x, h/2.0 + y)
		};
		return polygon;
	}
}
