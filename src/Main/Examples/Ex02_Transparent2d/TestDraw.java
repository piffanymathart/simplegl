package Examples.Ex02_Transparent2d;

import SimpleGL.GLSetupHelper;
import SimpleGL.SimpleDrawGL;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

/**
 * Setup: 2D setup
 * Draw: Three overlapping translucent 2D triangles.
 */
class TestDraw extends SimpleDrawGL {

	private TestData m_data;

	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		m_data.setWidth(width);
		m_data.setHeight(height);
		m_data.setDpiScale(dpiScale);
	}

	@Override
	public void setup(GL2 gl2) {

		GLSetupHelper.enableTranslucency(gl2);
		GLSetupHelper.enableAntialiasing(gl2);
		GLSetupHelper.set2DMode(gl2, m_data.getWidth(true), m_data.getHeight(true));
	}

	@Override
	public void draw(GL2 gl2) {

		gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
		gl2.glLoadIdentity();

		int w = m_data.getWidth(true);
		int h = m_data.getHeight(true);

		drawTriangle(gl2, w/2.0, h/2.0, w, h);
		drawTriangle(gl2, w/4.0, h/2.0, w/2.0, h/2.0);
		drawTriangle(gl2, w*0.75, h/3.0, w/3.0, h);
		drawTriangle(gl2, 400, 400, 400, 400);
	}

	/**
	 * Draws a 2D triangle given the centre position and the dimension.
	 * @param gl2 The GL2 object.
	 * @param x The x-value of the centre.
	 * @param y The y-value of the centre.
	 * @param w The width of the triangle.
	 * @param h The height of the triangle.
	 */
	private void drawTriangle(GL2 gl2, double x, double y, double w, double h) {

		gl2.glBegin( GL.GL_TRIANGLES );
		gl2.glColor4d( 1, 0, 0, 0.5 );
		gl2.glVertex2d( x-w/2, y-h/2 );
		gl2.glColor4d( 0, 1, 0, 0 );
		gl2.glVertex2d( x+w/2, y-h/2 );
		gl2.glColor4d( 0, 0, 1, 1 );
		gl2.glVertex2d( x, y+h/2 );
		gl2.glEnd();
	}
}
