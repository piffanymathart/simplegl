package Examples.Ex08_SelfIntersecting2d;

import SimpleGL.SimpleCanvasGL;
import SimpleGL.SimpleDrawGL;
import SimpleGL.SimpleFrameGL;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Scene: Self-intersecting 2D polygon.
 */
class TestMain {

	public static void main(String[] args) {

		String title = "Test GL App";
		TestData data = new TestData();

		Map<Component, String> layoutComponentMap = new HashMap<>();


		SimpleDrawGL draw = new TestDraw(data);
		SimpleCanvasGL canvas = new SimpleCanvasGL(draw);
		layoutComponentMap.put( canvas, BorderLayout.CENTER );

		new SimpleFrameGL(title, new int[] {data.getWidth(true), data.getHeight(true)}, layoutComponentMap);
	}
}
