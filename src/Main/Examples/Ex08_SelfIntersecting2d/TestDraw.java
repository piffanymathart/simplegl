package Examples.Ex08_SelfIntersecting2d;

import SimpleGL.GLDrawHelper;
import SimpleGL.GLSetupHelper;
import SimpleGL.SimpleDrawGL;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import javax.vecmath.Point2d;

/**
 * Setup: 2D setup
 * Draw: A self-interacting polygon.
 */
class TestDraw extends SimpleDrawGL {

	/**
	 * The data object.
	 */
	private TestData m_data;

	/**
	 * Constructs from data.
	 * @param data The data object.
	 */
	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		m_data.setWidth(width);
		m_data.setHeight(height);
		m_data.setDpiScale(dpiScale);
	}

	@Override
	public void setup(GL2 gl2) {

		GLSetupHelper.enableTranslucency(gl2);
		GLSetupHelper.enableAntialiasing(gl2);
		GLSetupHelper.set2DMode(gl2, m_data.getWidth(true), m_data.getHeight(true));
	}

	@Override
	public void draw(GL2 gl2) {

		gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
		gl2.glLoadIdentity();

		Point2d[] polygon = m_data.getPolygon();
		double[] fc = m_data.getFillColour();
		double[] sc = m_data.getStrokeColour();
		gl2.glColor4d( fc[0], fc[1], fc[2], fc[3] );
		GLDrawHelper.fillPolygon2d(gl2, polygon);
		gl2.glColor4d( sc[0], sc[1], sc[2], sc[3] );
		GLDrawHelper.drawPolygon2d(gl2, polygon);
	}
}
