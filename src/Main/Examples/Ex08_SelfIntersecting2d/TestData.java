package Examples.Ex08_SelfIntersecting2d;

import javax.vecmath.Point2d;

/**
 * Contains data to be drawn.
 */
class TestData {

	/**
	 * The polygon.
	 */
	private Point2d[] m_polygon;

	/**
	 * The fill colour.
	 */
	private double[] m_fillColour;

	/**
	 * The stroke colour.
	 */
	private double[] m_strokeColour;

	/**
	 * The canvas width.
	 */
	private int m_width;

	/**
	 * The canvas height.
	 */
	private int m_height;

	/**
	 * The DPI scale.
	 */
	private double m_dpiScale = 1;

	/**
	 * Default constructor to initialize the data.
	 */
	TestData() {

		m_width = 640;
		m_height = 480;

		// figure eight shape
		m_polygon = new Point2d[] {
			new Point2d(10, 400),
			new Point2d(400, 400),
			new Point2d(10, 10),
			new Point2d(400, 10)
		};

		m_fillColour = new double[] {1, 0, 0, 0.2};

		m_strokeColour = new double[] {1, 0, 0, 0.7};
	}

	/**
	 * Gets the canvas width.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The width.
	 */
	int getWidth(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_width*m_dpiScale) : m_width;
	}

	/**
	 * Gets the canvas height.
	 * @param withDpiScaling whether DPI scaling is included.
	 * @return The height.
	 */
	int getHeight(boolean withDpiScaling) {
		return withDpiScaling ? (int) (m_height*m_dpiScale) : m_height;
	}

	/**
	 * Sets the canvas width.
	 * @param width The width.
	 */
	void setWidth(int width) { m_width = width; }

	/**
	 * Sets the canvas height.
	 * @param height The height.
	 */
	void setHeight(int height) { m_height = height; }

	/**
	 * Gets the DPI scale.
	 * @return The DPI scale.
	 */
	double getDpiScale() { return m_dpiScale; }

	/**
	 * Sets the DPI scale.
	 * @param dpiScale The DPI scale.
	 */
	void setDpiScale(double dpiScale) { m_dpiScale = dpiScale; }

	/**
	 * Gets the polygon.
	 * @return The polygon.
	 */
	Point2d[] getPolygon() {
		return m_polygon;
	}

	/**
	 * Gets the fill colour.
	 * @return The fill colour.
	 */
	double[] getFillColour() {
		return m_fillColour;
	}

	/**
	 * Gets the stroke colour.
	 * @return The stroke colour.
	 */
	double[] getStrokeColour() {
		return m_strokeColour;
	}
}
