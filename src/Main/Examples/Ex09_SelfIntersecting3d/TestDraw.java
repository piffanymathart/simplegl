package Examples.Ex09_SelfIntersecting3d;

import SimpleGL.GLDrawHelper;
import SimpleGL.GLSetupHelper;
import SimpleGL.SimpleDrawGL;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import javax.vecmath.Point3d;

/**
 * Setup: 3D setup
 * Draw: A self-interacting polygon.
 */
class TestDraw extends SimpleDrawGL {

	/**
	 * The data object.
	 */
	private TestData m_data;

	/**
	 * Constructs from data.
	 * @param data The data object.
	 */
	TestDraw(TestData data) {
		m_data = data;
	}

	@Override
	public void setSize(int width, int height, double dpiScale) {
		m_data.setWidth(width);
		m_data.setHeight(height);
		m_data.setDpiScale(dpiScale);
	}

	@Override
	public void setup(GL2 gl2) {

		GLSetupHelper.enableTranslucency(gl2);
		GLSetupHelper.enableAntialiasing(gl2);
		GLSetupHelper.set3DMode(gl2, m_data.getWidth(true), m_data.getHeight(true), Math.PI/180.0*35.0);
	}

	@Override
	public void draw(GL2 gl2) {

		gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
		gl2.glLoadIdentity();

		Point3d[] polygon = m_data.getPolygon();
		double[] fc = m_data.getFillColour();
		double[] sc = m_data.getStrokeColour();
		gl2.glColor4d( fc[0], fc[1], fc[2], fc[3] );
		GLDrawHelper.fillPolygon3d(gl2, polygon);
		gl2.glColor4d( sc[0], sc[1], sc[2], sc[3] );
		GLDrawHelper.drawPolygon3d(gl2, polygon);
	}
}
