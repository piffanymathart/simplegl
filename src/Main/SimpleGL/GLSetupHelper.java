package SimpleGL;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static com.jogamp.opengl.GL.*;

/**
 * A helper class to facilitate canvas set up.
 */
public class GLSetupHelper {

	/**
	 * Clears the canvas by filling it with its background colour.
	 * @param gl2 The GL2 object.
	 * @param bgColor The background colour.
	 */
	public static void clearCanvas(GL2 gl2, float[] bgColor) {
		gl2.glClearColor(bgColor[0], bgColor[1], bgColor[2], bgColor[3]);
		gl2.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	/**
	 * Enables translucent colours.
	 * @param gl2 The GL2 object.
	 */
	public static void enableTranslucency(GL2 gl2) {
		gl2.glEnable(GL_BLEND);
		gl2.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	/**
	 * Enables antialiasing.
	 * @param gl2 The GL2 object.
	 */
	public static void enableAntialiasing(GL2 gl2) {
		gl2.glEnable(GL.GL_LINE_SMOOTH);
		gl2.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
	}

	/**
	 * Prepares canvas for 2D drawing mode.
	 * @param gl2 The GL2 object.
	 * @param width The canvas width.
	 * @param height The canvas height.
	 */
	public static void set2DMode(GL2 gl2, int width, int height) {

		gl2.glMatrixMode( GL2.GL_PROJECTION );
		gl2.glLoadIdentity();

		GLU glu = new GLU();
		glu.gluOrtho2D( 0.0, width, 0.0, height ); // origin at lower left

		gl2.glMatrixMode( GL2.GL_MODELVIEW );
		gl2.glLoadIdentity();

		gl2.glViewport( 0, 0, width, height );
	}

	/**
	 * Prepares canvas for 3D drawing mode.
	 * @param gl The GL object.
	 * @param width The canvas width.
	 * @param height The canvas height.
	 * @param fovY The vertical field of view in radians for the canvas.
	 */
	public static void set3DMode(GL gl, int width, int height, double fovY) {
		set3DMode(
			gl,
			width,
			height,
			fovY,
			new Point3d(0,0,0),
			new Point3d(0,0,-1),
			new Vector3d(0,1,0)
		);
	}

	/**
	 * Prepares canvas for 3D drawing mode.
	 * @param gl The GL object.
	 * @param width The canvas width.
	 * @param height The canvas height.
	 * @param fovY The vertical field of view in radians for the canvas.
	 * @param eye The eye position.
	 * @param lookAt The position to look at.
	 * @param up The up vector.
	 */
	public static void set3DMode(GL gl, int width, int height, double fovY, Point3d eye, Point3d lookAt, Vector3d up) {
		GL2 gl2 = gl.getGL2();

		// Set the current view port to cover full screen
		gl2.glViewport(0, 0, width, height);

		// Set up the projection matrix - choose perspective view
		gl2.glMatrixMode(GL2.GL_PROJECTION);
		gl2.glLoadIdentity(); // reset
		GLU glu = new GLU();
		// fovy, aspect, zNear, zFar
		glu.gluPerspective(
			fovY/Math.PI*180.0,
			(double)width/(double)height,
			1e-5,
			1e9
		);
		// (eye.x, eye.y, eye.z, target.x, target.y, target.z, up.x, up.y, up.z)
		glu.gluLookAt(eye.x, eye.y, eye.z, lookAt.x, lookAt.y, lookAt.z, up.x, up.y, up.z);

		// Enable the model-view transform
		gl2.glMatrixMode(GL2.GL_MODELVIEW);
		gl2.glLoadIdentity();
	}
}
