package SimpleGL;

import com.jogamp.opengl.GL2;

/**
 * A simple interface for drawing on a GL canvas.
 *
 * Usage: Override drawing methods by subclassing.
 */
public abstract class SimpleDrawGL {

	/**
	 * Sets the canvas width and height.
	 * @param width The canvas width.
	 * @param height The canvas height.
	 * @param dpiScale The DPI scale.
	 */
	public abstract void setSize(int width, int height, double dpiScale);

	/**
	 * Setup method for canvas.
	 * @param gl2 The GL2 object.
	 */
	public abstract void setup(GL2 gl2);

	/**
	 * Main drawing method for canvas.
	 * @param gl2 The GL2 object.
	 */
	public abstract void draw(GL2 gl2);
}
