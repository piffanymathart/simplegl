package SimpleGL;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;


/**
 * A simple frame to hold various UI components.
 */
public class SimpleFrameGL extends JFrame {

	/**
	 * Basic constructor.
	 * @param title Frame title.
	 * @param size Frame size (includes DPI scaling)
	 * @param layoutComponentMap A mapping of component to BorderLayout constant.
	 */
	public SimpleFrameGL(
		String title,
		int[] size,
		Map<Component, String> layoutComponentMap
	) {
		super( title );

		addWindowListener( new WindowAdapter() {
			public void windowClosing( WindowEvent windowevent ) {
				dispose();
				System.exit( 0 );
			}
		});

		for(Map.Entry<Component, String> entry : layoutComponentMap.entrySet()) {
			add(entry.getKey(), entry.getValue());
		}

		setSize( size[0], size[1] );
		setMinimumSize(new Dimension(size[0], size[1]));
		pack();

		setVisible( true );
	}
}
