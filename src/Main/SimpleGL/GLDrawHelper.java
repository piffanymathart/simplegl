package SimpleGL;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUtessellator;

import javax.vecmath.Point2d;
import javax.vecmath.Point3d;

/**
 * A helper class for drawing shapes (especially concave and self-intersecting polygons).
 */
public class GLDrawHelper {

	/**
	 * Draws the outlines of a 2D polygon.
	 * @param gl2 The GL2 object.
	 * @param polygon The vertices of the polygon.
	 */
	public static void drawPolygon2d(GL2 gl2, Point2d[] polygon) {
		gl2.glBegin( GL.GL_LINE_LOOP );
		for(Point2d p : polygon) {
			gl2.glVertex2d( p.x, p.y );
		}
		gl2.glEnd();
	}

	/**
	 * Draws the outlines of a 3D polygon.
	 * @param gl2 The GL2 object.
	 * @param polygon The vertices of the polygon.
	 */
	public static void drawPolygon3d(GL2 gl2, Point3d[] polygon) {
		gl2.glBegin( GL.GL_LINE_LOOP );
		for(Point3d p : polygon) {
			gl2.glVertex3d( p.x, p.y, p.z );
		}
		gl2.glEnd();
	}

	/**
	 * Fills the interior of a 2D polygon.
	 * @param gl2 The GL2 object.
	 * @param polygon The vertices of the polygon.
	 */
	public static void fillPolygon2d(GL2 gl2, Point2d[] polygon) {

		// source: https://github.com/sgothel/jogl-demos/blob/master/src/demos/tess/Tess.java

		if(polygon.length<3) return;

		GLUtessellator tobj = GLU.gluNewTess();
		TessCallback tessCallback = new TessCallback(gl2, new GLU());

		GLU.gluTessCallback(tobj, GLU.GLU_TESS_VERTEX, tessCallback);
		GLU.gluTessCallback(tobj, GLU.GLU_TESS_BEGIN, tessCallback);
		GLU.gluTessCallback(tobj, GLU.GLU_TESS_END, tessCallback);
		GLU.gluTessCallback(tobj, GLU.GLU_TESS_COMBINE, tessCallback);

		gl2.glShadeModel(GL2.GL_SMOOTH);
		GLU.gluTessProperty(tobj, GLU.GLU_TESS_WINDING_RULE, GLU.GLU_TESS_WINDING_NONZERO);
		GLU.gluTessBeginPolygon(tobj, null);
		GLU.gluTessBeginContour(tobj);
		for(Point2d p : polygon) {
			GLU.gluTessVertex(tobj, new double[] {p.x, p.y,0}, 0, new double[] {p.x, p.y,0});
		}
		GLU.gluTessEndContour(tobj);
		GLU.gluTessEndPolygon(tobj);

		GLU.gluDeleteTess(tobj);

		gl2.glFlush();
	}

	/**
	 * Fills the interior of a 3D polygon.
	 * @param gl2 The GL2 object.
	 * @param polygon The vertices of the polygon.
	 */
	public static void fillPolygon3d(GL2 gl2, Point3d[] polygon) {

		// source: https://github.com/sgothel/jogl-demos/blob/master/src/demos/tess/Tess.java
		if(polygon.length<3) return;

		GLUtessellator tobj = GLU.gluNewTess();
		TessCallback tessCallback = new TessCallback(gl2, new GLU());

		GLU.gluTessCallback(tobj, GLU.GLU_TESS_VERTEX, tessCallback);
		GLU.gluTessCallback(tobj, GLU.GLU_TESS_BEGIN, tessCallback);
		GLU.gluTessCallback(tobj, GLU.GLU_TESS_END, tessCallback);
		GLU.gluTessCallback(tobj, GLU.GLU_TESS_COMBINE, tessCallback);

		gl2.glShadeModel(GL2.GL_SMOOTH);
		GLU.gluTessProperty(tobj, GLU.GLU_TESS_WINDING_RULE, GLU.GLU_TESS_WINDING_NONZERO);
		GLU.gluTessBeginPolygon(tobj, null);
		GLU.gluTessBeginContour(tobj);
		for(Point3d p : polygon) {
			GLU.gluTessVertex(tobj, new double[] {p.x, p.y, p.z}, 0, new double[] {p.x, p.y, p.z});
		}
		GLU.gluTessEndContour(tobj);
		GLU.gluTessEndPolygon(tobj);

		GLU.gluDeleteTess(tobj);

		gl2.glFlush();
	}

	/**
	 * A helper class to do triangulation callback.
	 */
	private static class TessCallback extends com.jogamp.opengl.glu.GLUtessellatorCallbackAdapter {
		GL2 gl;
		GLU glu;
		int vertexIndex = 0;

		private TessCallback(GL2 gl, GLU glu) {
			this.gl = gl;
			this.glu = glu;
		}

		public void begin(int type) {
			gl.glBegin(type);
		}

		public void end() {
			gl.glEnd();
		}

		public void vertex(Object data) {
			if (data instanceof double[]) {
				double[] d = (double[]) data;
				if (d.length == 6) {
					gl.glColor3dv(d, 3);
				}
				gl.glVertex3dv(d, 0);
			}
		}

		public void error(int errnum) {
			String estring;
			estring = glu.gluErrorString(errnum);
			System.out.println("Tessellation Error: " + estring);
			System.exit(0);
			throw new RuntimeException();
		}

		public void combine(double[] coords, Object[] data,
		                    float[] weight, Object[] outData) {

			double[] vertex = new double[data.length];
			for(int i=0; i<coords.length; i++) vertex[i] = coords[i];
			outData[0] = vertex;
		}
	}
}
