package SimpleGL;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import java.awt.*;

/**
 * A simple CanvasGL that handles window events and DPI scaling.
 */
public class SimpleCanvasGL extends GLCanvas {

	/**
	 * The draw object.
	 */
	private SimpleDrawGL m_draw;

	/**
	 * Constructor from draw object.
	 * @param draw The draw object.
	 */
	public SimpleCanvasGL(SimpleDrawGL draw) {

		super( getCapabilities() );
		m_draw = draw;
		setFocusable(true);
		addGLEventListener(new SimpleGLEventListener());
	}

	/**
	 * Gets the DPI scaling factor.
	 * @return The DPI scaling factor.
	 */
	private double getDpiScale() {
		double dpiScale = ((Graphics2D) getGraphics()).getTransform().getScaleX();
		return dpiScale;
	}

	/**
	 * Creates the GL capabilities used to initialize the canvas.
	 * @return The GL capabilities.
	 */
	private static GLCapabilities getCapabilities() {
		GLProfile glprofile = GLProfile.get(GLProfile.GL2);
		GLCapabilities glcapabilities = new GLCapabilities( glprofile );
		glcapabilities.setDoubleBuffered(true);
		return glcapabilities;
	}

	/**
	 * A simple listener for GL events.
	 */
	private class SimpleGLEventListener implements GLEventListener {

		@Override
		public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width, int height) {
			double dpiScale = getDpiScale();
			m_draw.setSize(width, height, dpiScale);
			m_draw.setup(glautodrawable.getGL().getGL2());
			m_draw.draw(glautodrawable.getGL().getGL2());
		}

		@Override
		public void init(GLAutoDrawable glautodrawable) {
			double dpiScale = getDpiScale();
			m_draw.setSize(
				glautodrawable.getSurfaceWidth(),
				glautodrawable.getSurfaceHeight(),
				dpiScale
			);
		}

		@Override
		public void dispose(GLAutoDrawable glautodrawable) {
		}

		@Override
		public void display(GLAutoDrawable glautodrawable) {
			double dpiScale = getDpiScale();
			m_draw.setSize(
				glautodrawable.getSurfaceWidth(),
				glautodrawable.getSurfaceHeight(),
				dpiScale
			);
			m_draw.draw(glautodrawable.getGL().getGL2());
		}
	}
}
